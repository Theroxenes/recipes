# General Notes and Tips
## Notes:
All recipes in this list use [baker's percentage](https://www.kingarthurbaking.com/pro/reference/bakers-percentage) and metric weight measure.
A word of warning, leavened bread is highly dependent on environmental conditions for times, signals, and generally working correctly.  
It takes some practice and familiarity with how heat, moisture, etc. affect fermentation time and properties of the bread.  



### Equipment
Some kitchen gear that you may or may not have is borderline necessary for consistent and pleasant bread baking:
1. A kitchen scale with metric measures that can handle both large weights and do decently fine measurements.
2. Proofing baskets/bannetons. Bowls or other containers can be used but they are not great to work with.
3. Razor blades and a lame or holder, see [scoring](#user-content-scoring).
4. Either a professional stone-floor oven (lol) or a baking stone/steel, see [ovens and baking](#user-content-ovens-and-baking). This one is the least necessary but is extremely nice for higher volume bakes.
5. Depending on your environment and the season, a proofing box might be a good invesment. They are quite costly, but there are several DIY solutions<sup>[\[1\]](https://www.youtube.com/watch?v=yVYWMrBOL6c)[\[2\]](https://www.youtube.com/watch?v=XGWiu62pHd8)</sup> available if that's more your speed. I may make my own guide on this at some point.    



### Ovens and Baking
Depending on the type of oven used, different baking techniques and tricks might be needed.  
Convection ovens are particularly common and challenging, as the air circulation fan will suck out the steam needed for a good bake. A dutch oven, baking bag, or foil tent can be used to trap the steam if the convection fan can't be disabled.  
If not using a smaller container like a dutch oven, additional steam should be added by spraying the walls of the oven with water, and/or placing a small oven-safe container of water below your baking surface.

For most home ovens, a preheated baking stone or steel is ideal, but not necessary. Standard baking trays or even a cast iron pan will give decent results.  



## Sourdough Starter
There are a number of ways to maintain and feed a sourdough starter, but they all begin the same way.  
Consider the size of the jar you are storing it in primarily, as well as your baking schedule when deciding how to handle a starter.  
Make sure your starter vessel has a lid that can loosely fit, and enough room for your final batch size.  

Below is my recommended method, as well as some alternatives that may work better for you situation:  

1. Mix 20g whole wheat flour + 20g warm water in your vessel.
    - To save on flour, it's advisable to keep starter small at the start.
    - As a result you might want to start it in a smaller vessel and move to your final container later on.
2. Set in a warm (70-80F) location for 24 hours.
3. 2nd day, discard 20g of the mixture and add 10g white flour + 10g warm water.
4. Repeat this process through day 5. At this point, or even earlier, the starter should be expanding a great deal by the end of the 24 hour rest period and it should have a sharp tangy/sour smell.
    - Small amounts of brownish liquid may form on the surface. This is basically alcohol and should be discarded as much as possible.
5. On day 6, instead of discarding add 20g white flour and 20g warm water.
6. On day 7, add 40g white flour and 40g warm water, bringing total mass to 80g flour + 40g water.
7. At the start of day 8, the starter should be foaming significantly and ready to be used or fed and stored in the fridge.

Starter should be fed or used weekly. Well before use, add 2% baker's percentage starter to a separate vessel and 9% water, 9% flour. Allow to double in size before use.    
An active starter should look something like this:  
<img src='./images/starter_side.jpg' alt='Starter Sideview' width=40%/>  
<img src='./images/starter_top.jpg' alt='Starter Topview' width=40%/>  
<sup>This 22oz jar is actually barely large enough for the 400g starter shown here. I'd recommend a slightly larger one for this size.</sup>

While I can't represent the target smell in this recipe, it should be fairly obvious if it's *off* in a bad way.  
As long as that doesn't happen, the worst that can happen is not enough bacteria producing the sourness of your sourdough, which is sad but not fatal!  



### Alternate Feeding Methods
Depending on your baking schedule and volume, alternate methods of maintaining a starter might be more cost-effective. The traditional method outlined above works well for me baking a couple of loaves once or twice a week, but if you find yourself throwing out large volumes of starter after the initial activation period, consider one of the alternatives:

1. "Scraps"/"scrapings" method:
    - Instead of using half the starter for the dough, use almost all of it leaving behind a small amount of scrap starter in your container. Preserve the scraps in the fridge until the night before your next bake, at which point you will add slightly more flour + water by weight than starter you will use for your bake (e.g. for a 1kg dough @ 20% starter, add ~110g water + ~110g flour to your starter).
2. Drying method:
    - Note, I have not tried this, but supposedly by spreading a thin layer of starter onto a sheet or pan and allowing it to dry at room temperature, you can essentially create your own active dry yeast which keeps much longer. Probably best for very spread out baking times rather than a consistent or high volume schedule.
3. The eternal dough/levain:
    - Use all of your starter for your dough, and at the end of bulk fermentation tear a small amount off and preserve it as your new starter. This is actually an even more traditional method, but it has its drawbacks as you'll be introducing salt and potentially more environmental gunk into your starter. Potentially not great if you use a wide variety of flours in your doughs as they will all end up mixed into your starter, but it is efficient and low maintenance especially with modern refrigeration.



## Scoring
Scoring is the process of cutting slits in the top of loaves before baking to allow them to expand and steam to escape.  
It's critical for well-shaped loaves: without scoring, they will still burst open, just in more or less random places leaving you with an extremely uneven and unappealing looking lump of bread.  
It will still taste good if you do everything else correctly, but mastering scoring is key to *pretty* loaves of artisinal bread.  

The ideal tool for scoring is a [bread lame](https://www.kingarthurbaking.com/blog/2017/08/04/scoring-bread-dough) + double-sided razor blade.  
A coffee stirrer can be used as a makeshift lame in a pinch, or you can even carve your own:  
<img src='./images/ghetto_lame.jpg' alt='I do not recommend this' width=40%/>  
<sup>Don't do this at home kids.</sup>

As noted in the King Arthur article, a sharp pair of scissors can also be used.  
If you can't get a razor and don't want to use scissors, a thin blade such as a filet knife can be used if it's sharp enough, but it really isn't ideal.

For technique, keep a few things in mind:
1. You want at least 1 deep, central score for most patterns. You should certainly start out doing this. It gives the loaf plenty of room to expand and puff out of the score. This score should be done in one pass, going over it again can disrupt the structure of your loaf. It takes a bit of practice!
    - The depth depends a bit on the thickness of the loaf, but around 1/4" is a good target.
    - For some fancier patterns you might want to do several slightly shallower cuts as your "primary" expansion zone. Scoring all the way around a square (or whatever shape) section of the loaf will also achieve this expansion zone effect, even if the cuts are quite shallow.
2. Decorative scores should be more shallow and typically shorter. How you do these is really up to you, play around with it and try stuff out. They aren't structurally that important.
3. Thick "ears" of crust will form at the edges of your scores, and if they are too thick they can get in the way of slicing.



# Bread Recipes
## Hybrid Sourdough
A light, fluffy, and forgiving dough that packs a lot of flavor.  
This recipe uses a base of 800g flour (70%/30% white bread flour and whole wheat) but can be scaled to any size.  

### Making the dough.
1. Before beginning, put 15g of starter in a jar or other sealable container. Add 45g of water and 45g of flour.
2. In a large metal or glass bowl, sift or stir together 560g [white bread flour](https://shop.kingarthurbaking.com/items/bread-flour) and 240g [whole wheat bread flour](https://shop.kingarthurbaking.com/items/100-whole-wheat-flour) or [white whole wheat bread flour](https://www.kingarthurbaking.com/learn/guides/white-whole-wheat).
3. Add 65% (520g) room-temperature water to the flour along with ~18g (2.25%) salt to the flour and mix until it is homogenous. Allow the dough to autolyze until the starter is roughly doubled in size.
4. Mix the autolyze and starter together until fully incorporated.
    - Or, if you're using a mixer just mix it starting on low until fully combined.
5. Turn dough onto a floured work surface and knead until the dough becomes smooth and passes the [windowpane test](https://www.kingarthurbaking.com/blog/2022/10/14/what-is-the-windowpane-test-for-bread-dough) (or, if using a stand mixer, mix on high for ~5 minutes until it passes the windowpane test).
    - If working in a cold room, or if the starter seems inactive, add 1% (~2g) of active dry yeast at this stage. The starter bacteria will still contribute flavor.
    - The high amount of whole-wheat flour can make the windowpane test a bit different, as the shards of bran disrupt the gluten structure. It's ok if it breaks apart a little.
7. Split off a small piece of the final dough and reserve in a jar or clear container marked at the level of the dough piece. Use this to judge the rise of the dough. Place the rest of the dough in a container, cover and [bulk ferment](https://www.kingarthurbaking.com/blog/2019/07/22/bread-dough-bulk-fermentation) in a warm room. This bulk fermentation time can vary widely depending on the ambient temperature and humidity! At 80F with this formula, it takes roughly 5.5 hours to fully proof.
8. Every ~30 minutes or so, perform stretch and folds on the proofing dough, gently lifting it and tucking the edges underneath to futher knead and develop the gluten. Do this 4 or 5 times in total.
8. Flour and gently remove the dough from the container onto a floured work surface. Split in half and weigh the final loaves out, each should weigh roughly 775g.
9. [Pre-shape](https://www.kingarthurbaking.com/blog/2019/02/08/preshaping-bread-dough) the pieces and allow to rest until relaxed (~15 minutes).
10. Perform the final shaping of the dough, then place **seam-up** into proofing baskets/bannetons lightly floured with all-purpose flour.
    - Shaping method depends on the basket shape, for boules fold edges into the center then roll in with hands, and pinch off the seam.
    - For loaf-shapes, fold the edges into the center in a more rectangular shape, pinch off the seam, then roll on your work surface to thin the ends.
11. Cover baskets and either allow to rise, or store overnight in the fridge and warm them back up slightly before baking. Cooled loaves will be easier to score cleanly, but with practice and a sharp blade room-temp is no problem.   

### Baking
1. Preheat an oven to 450-475F with your baking surface (stone/steel, cast iron, or baking tray) in the center rack. See [baking tips](#user-content-ovens-and-baking) for more info.
    - If you have one, use an infrared thermometer to make sure the baking stone or steel is fully up to temp before baking.
2. Once the oven is up to temp, turn the loaves out onto a lightly floured peel and [score](#user-content-scoring) the top with a razor or extremely sharp, thin knife.
3. Slide the loaves onto the baking surface and spray the walls of the oven with a few spritzes of water from a spray bottle or hose.
4. Set a timer for ~25 minutes. Avoid opening the oven door and letting the steam out, the loaves should have a little color and be almost fully firmed up at the end of this first baking step.
5. If using a water tray, remove the tray, and bake for ~10 more minutes until the bottom is crisp and it passes the [knock test](https://www.kingarthurbaking.com/blog/2023/05/31/how-to-tell-if-bread-is-done).
    - If baking smaller loaves, this step may be unnecessary and baking times will of course vary.
6. Remove the loaves from the oven and allow to cool completely directly on a wire rack or in a basket.  



## Simple White Loaf
1. 450g Special Patent Flour
2. 300 water
3. 10g salt
4. ~1tsp active dry yeast



## Pizza Dough
Ingredients:
1. 450g white flour
2. 315g (70%) water
3. 10g (~2%) salt
4. ~1tsp active dry yeast  

1. Mix 100g flour, 100g water, and the yeast in a bowl as a preferment.
2. Let the preferment develop for ~30 minutes until bubbles begin to form and it starts to smell of yeast.
3. Mix in the rest of the flour, water, and the salt.
4. Knead until it becomes smooth and tacky.
5. Allow to rise until doubled in size.
6. Divide into 2 balls (~390g each) and place into lightly oiled containers.
7. Allow to rise for ~45 more minutes until doubled in size again.
8. Flatten balls out and stretch into pizza rounds. Top and bake at 550F for 6-8 minutes on a preheated pizza stone or steel.
