# Chocolate Recipes  

## Childhood Chocolate Sauce  

### Ingredients  
1. 1/4 cup butter
2. 2 ounces unsweetened chocolate
3. 1/4 cup cocoa powder
4. 1/2 cup sugar
5. 1/2 cup evaporated milk
6. 1/2 tsp vanilla extract  

### How to Make
1. In a pot, melt butter and unsweetened chocolate on low heat.
2. Add remaining ingredients and stir constantly until smooth.
3. Allow to cool and enjoy.
