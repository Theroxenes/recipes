# Cookie Recipes  

## Pumpkin Chocolate Chip Cookies  

### Ingredients

1. 1 can of pureed pumpkin
2. 240g flour
3. 1 tsp cinnamon
4. 1 tsp vanilla
5. 1 tsp baking soda
6. 1 tsp baking powder
7. 1/4 tsp salt
8. 200g sugar
9. 113g butter (1 stick) or coconut oil
10. Enough chocolate chips (around a cup)  

### How to Make Them
1. Preheat oven to 350 degrees.
2. Cream the sugar and butter/oil together in a large bowl.
3. Add the pumpkin and vanilla, and mix until smooth.
5. Sift the remaining dry ingredients (flour, cinnamon, baking soda, salt) in.
6. Lightly mix until mostly combined, then add the chocolate chips and finish mixing.
7. Dollop onto a parchment-paper lined baking tray and bake for 10-15 minutes until golden brown and crispy edges start forming.
8. Let cool completely.  



## Chocolate Chip Brown Butter Cookies

### Notes  
Based on [this recipe courtesy of Sally's Baking Addiction](https://sallysbakingaddiction.com/chewy-chocolate-chip-cookies/) slightly modified by myself.  
The original recipe is excellent, and this slightly more involved version adds a layer of caramel nutty deliciousness with the browned butter and richer brown sugar mix.  

### Ingredients  
1. 0.75 (3/4) cups of unsalted butter (browned)  
2. 210g of white granulated sugar  
3. 40g of molasses  
4. 2 medium-sized eggs  
5. 2tsp vanilla extract  
6. 280g (2.25 cups) AP flour  
7. 1 tsp baking soda  
9. 1.5 tsp cornstarch  
9. 0.5 tsp salt
10. 225g (1.25 cups) semi-sweet chocolate chips  

### How to Make Them  
1. Brown the butter by simmering on low heat for ~5-10 minutes until foaming mostly stops, and the milk solids drop out taking on a golden-brown color.  
    a. You can omit browning the butter, if you do use 1 egg or 1 egg + 1 egg yolk as in the original recipe. Browning the butter removes a significant amount of water.  
2. While butter is browning thoroughly combine granulated sugar and molasses in a large bowl.  
    a. Sugar/molasses can be substituted for 150g brown sugar + 100g white sugar as in the original recipe.  
3. Combine brown butter and homemade brown sugar mix and allow to cool. It will look like an oily mess, this is fine.    
4. Add eggs and vanilla to sugar/butter mixture and stir thoroughly.  
5. Mix dry ingredients (flour, baking soda, cornstarch, salt) separately then mix into wet ingredients until mostly combined.  
6. Add chocolate chips and finish mixing until no white flour is visible.  
7. Allow dough to cool in the fridge while preheating oven to 325F.  
8. Dollop heaping tablespoons of dough (piled high) onto a parchment-lined baking sheet and bake for ~15 minutes until golden.  
9. Cool baked cookies on a wire rack.  
